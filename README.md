# UltraSound ToolBox (USTB) #

An open source MATLAB toolbox for beamforming, processing, and visualization of ultrasonic signals. The USTB is developed as a joint effort of:
 
* [Department of Circulation and Medical Imaging of NTNU](https://www.ntnu.no/isb), 
* [Department of Informatics of the University of Oslo](http://www.uio.no/), and
* [CREATIS Laboratory of the University of Lyon](https://www.creatis.insa-lyon.fr/site7/en).

### How do I get set up? ###

* Just clone the repository and add the folder (without subfolders) to MATLAB's path

### Who do I talk to? ###

The project administrators are:

* Alfonso Rodriguez-Molares <alfonso.r.molares@ntnu.no>,
* Ole Marius Hoel Rindal <omrindal@ifi.uio.no>
* Olivier Bernard <olivier.bernard@insa-lyon.fr> 
 

Collaborators:

* Andreas Austeng 
* Arun Nair
* Muyinatu A. Lediju Bell, 
* Lasse Løvstakken 
* Svein Bøe 
* Hervé Liebgott 
* Øyvind Krøvel-Velle Standal 
* Jochen Rau 
* Stefano Fiorentini